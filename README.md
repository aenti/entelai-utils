# entelai-utils #

This is a package where you can find utilities for different medical images formats (dicom, nifti)

Optional requirement: if you want to use nifti_to_dicom conversion please install nifti2dicom package.
`apt install nifti2dicom`

## Conversion ##
This package wraps multiple methods to perform conversions between formats.

### REQUIREMENTS ###
GDCM v2.8.8 is needed in order to entelaiutils work as intended. 

* Download your SO version from https://github.com/malaterre/GDCM/releases/tag/v2.8.8
* untar 
* add to `~/.zshrc` or `~/.bashrc` GDCM bin and lib paths: 
```bash

     export PATH="<YOUR_PATH>/GDCM-2.8.8-Linux-x86_64/bin:$PATH"
     export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:<YOUR_PATH>/GDCM-2.8.8-Linux-x86_64/lib"
```
* Re-open terminal or `source ~/.zshrc`



### nifti to dicom ###

Wrappers available:

* dicom2nifti
* dcm2niix
* mri_convert (**license.txt** from FreeSurfer needs to be added to *external* dir)

Usage:

```python
    >>> from entelaicripts import conversor
    >>> dicom_dir = '/home/user/3DT1/dicom_dir'
    >>> output_file = '/home/user/3DT1.nii.gz'
    >>> conversor.dicom_to_nifti(dicom_dir, output_file)
```

### dicom to nifti ###

Wrappers available:

* nifti2dicom

Usage:

```python
    >>> from entelaiutils import conversion
    >>> nifti_file = '/home/user/3DT1_aparc+aseg.nii.gz'
    >>> output_dicom_dir = '/home/user/3DT1/fs_dicom_dir'
    >>> dicom_header_file = '/home/user/3DT1/1.dcm
    >>> conversion.nifti_to_dicom(dicom_dir, output_file, dicomheaderfile=dicom_header_file)
```

### Calling a specific wrapper ###

If you want to use a wrapper directly you can call: *methodName*_wrapper() function.

Example:
```python
    >>> from entelaiutils import conversion
    >>> dicom_dir = '/home/user/3DT1/dicom_dir'
    >>> output_file = '/home/user/3DT1.nii.gz'
    >>> conversion.mri_convert_wrapper(dicom_dir, output_file)
```
