import pytest
import subprocess
from pydicom import Sequence
from pydicom.dataset import Dataset, FileMetaDataset
from mock import patch, Mock
from entelaiutils.conversion import dcm2png_convert4, dcm2png_convert3
from entelaiutils.conversion import dcm2png, ConversionError

def test_dcm2png_convert4_arguments():
    filename = 'ejemplo.dcm'
    with pytest.raises(AssertionError):
        assert dcm2png_convert4(filename, voi_lut_seq_or_window_number=0)


def test_dcm2png_convert4_VOILUTSequence():
    filename = 'ejemplo.dcm'
    output_fname = 'ejemplo.png'
    ds = Dataset()
    ds.file_meta = FileMetaDataset()
    ds.VOILUTSequence = Sequence([Dataset()])
    ds.VOILUTSequence[0].LUTDescriptor = [2906, 1189, 12]
    ds.VOILUTSequence[0].LUTExplanation = 'NORMAL'
    ds.VOILUTSequence[0].LUTData = [0, 0, 0]
    command = ['dcmj2pnm', '+on2', '+Wl', '1', filename, output_fname]
    spr_return = subprocess.CompletedProcess(args=command, returncode=0, stdout=b'', stderr=b'')

    with patch('entelaiutils.conversion.pydicom.dcmread', return_value=ds):
        with patch('entelaiutils.conversion.subprocess.run', return_value=spr_return):
            dcm2png_convert4(filename, voi_lut_seq_or_window_number=1)
            subprocess.run.assert_called_once_with(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def test_dcm2png_convert4_WindowCenterWidthExplanation():
    filename = 'ejemplo.dcm'
    output_fname = 'ejemplo.png'
    ds = Dataset()
    ds.file_meta = FileMetaDataset()
    ds.WindowCenterWidthExplanation = ['CURRENT', 'STANDARD', 'CONTRAST', 'SMOOTH', 'CUSTOM']
    command = ['dcmj2pnm', '+on2', '+Wi', '1', filename, output_fname]
    spr_return = \
        subprocess.CompletedProcess(args=command, returncode=0, stdout=b'', stderr=b'')

    with patch('entelaiutils.conversion.pydicom.dcmread', return_value=ds):
        with patch('entelaiutils.conversion.subprocess.run', return_value=spr_return):
            dcm2png_convert4(filename, voi_lut_seq_or_window_number=1)
            subprocess.run.assert_called_once_with(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

def test_dcm2png_convert4_Center_and_Width():
    filename = 'ejemplo.dcm'
    output_fname = 'ejemplo.png'
    ds = Dataset()
    ds.file_meta = FileMetaDataset()
    ds.WindowCenter = 2594.0
    ds.WindowWidth = 3592.0
    command = ['dcmj2pnm', '+on2', '+Ww', '2594.0', '3592.0', filename, output_fname]
    spr_return = subprocess.CompletedProcess(args=command, returncode=0, stdout=b'', stderr=b'')

    with patch('entelaiutils.conversion.pydicom.dcmread', return_value=ds):
        with patch('entelaiutils.conversion.subprocess.run', return_value=spr_return):
            dcm2png_convert4(filename, voi_lut_seq_or_window_number=1)
            subprocess.run.assert_called_once_with(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def test_dcm2png_convert4_no_tag_in_dcm():
    filename = 'ejemplo.dcm'
    output_fname = 'ejemplo.png'
    ds = Dataset()
    ds.file_meta = FileMetaDataset()
    ds.SeriesDescription = 'Test'
    command = ['dcmj2pnm', '+on2', filename, output_fname]
    spr_return = \
        subprocess.CompletedProcess(args=command, returncode=0, stdout=b'', stderr=b'')

    with patch('entelaiutils.conversion.pydicom.dcmread', return_value=ds):
        with patch('entelaiutils.conversion.subprocess.run', return_value=spr_return):
            dcm2png_convert4(filename, voi_lut_seq_or_window_number=1)
            subprocess.run.assert_called_once_with(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

def test_dcm2png_convert3_VOILUTSequence():
    filename = 'ejemplo.dcm'
    output_fname = 'ejemplo.png'
    ds = Dataset()
    ds.file_meta = FileMetaDataset()
    ds.VOILUTSequence = Sequence([Dataset()])
    ds.VOILUTSequence[0].LUTDescriptor = [2906, 1189, 12]
    ds.VOILUTSequence[0].LUTExplanation = 'NORMAL'
    ds.VOILUTSequence[0].LUTData = [0, 0, 0]
    command = ['dcm2pnm', '+on2', '+Wl', '1', filename, output_fname]
    spr_return = subprocess.CompletedProcess(args=command, returncode=0, stdout=b'', stderr=b'')

    with patch('entelaiutils.conversion.pydicom.dcmread', return_value=ds):
        with patch('entelaiutils.conversion.subprocess.run', return_value=spr_return):
            dcm2png_convert3(filename, voi_lut_seq_or_window_number=1)
            subprocess.run.assert_called_once_with(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def test_dcm2png_convert3_WindowCenterWidthExplanation():
    filename = 'ejemplo.dcm'
    output_fname = 'ejemplo.png'
    ds = Dataset()
    ds.file_meta = FileMetaDataset()
    ds.WindowCenterWidthExplanation = ['CURRENT', 'STANDARD', 'CONTRAST', 'SMOOTH', 'CUSTOM']
    command = ['dcm2pnm', '+on2', '+Wi', '1', filename, output_fname]
    spr_return = \
        subprocess.CompletedProcess(args=command, returncode=0, stdout=b'', stderr=b'')

    with patch('entelaiutils.conversion.pydicom.dcmread', return_value=ds):
        with patch('entelaiutils.conversion.subprocess.run', return_value=spr_return):
            dcm2png_convert3(filename, voi_lut_seq_or_window_number=1)
            subprocess.run.assert_called_once_with(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def test_dcm2png_convert3_Center_and_Width():
    filename = 'ejemplo.dcm'
    output_fname = 'ejemplo.png'
    ds = Dataset()
    ds.file_meta = FileMetaDataset()
    ds.WindowCenter = 2594.0
    ds.WindowWidth = 3592.0
    command = ['dcm2pnm', '+on2', '+Ww', '2594.0', '3592.0', filename, output_fname]
    spr_return = subprocess.CompletedProcess(args=command, returncode=0, stdout=b'', stderr=b'')

    with patch('entelaiutils.conversion.pydicom.dcmread', return_value=ds):
        with patch('entelaiutils.conversion.subprocess.run', return_value=spr_return):
            dcm2png_convert3(filename, voi_lut_seq_or_window_number=1)
            subprocess.run.assert_called_once_with(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def test_dcm2png_convert3_no_tag_in_dcm():
    filename = 'ejemplo.dcm'
    output_fname = 'ejemplo.png'
    ds = Dataset()
    ds.file_meta = FileMetaDataset()
    ds.SeriesDescription = 'Test'
    command = ['dcm2pnm', '+on2', filename, output_fname]
    spr_return = \
        subprocess.CompletedProcess(args=command, returncode=0, stdout=b'', stderr=b'')

    with patch('entelaiutils.conversion.pydicom.dcmread', return_value=ds):
        with patch('entelaiutils.conversion.subprocess.run', return_value=spr_return):
            dcm2png_convert3(filename, voi_lut_seq_or_window_number=1)
            subprocess.run.assert_called_once_with(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


@patch('entelaiutils.conversion.dcm2png_convert4', side_effect=ConversionError('Test'))
def test_dcm2png_VOILUTSequence_converter3(mocked_dcm2png_convert4):
    filename = 'ejemplo.dcm'
    ds = Dataset()
    ds.file_meta = FileMetaDataset()
    ds. Modality = 'MG'
    ds.VOILUTSequence = Sequence([Dataset()])
    ds.VOILUTSequence[0].LUTDescriptor = [2906, 1189, 12]
    ds.VOILUTSequence[0].LUTExplanation = 'NORMAL'
    ds.VOILUTSequence[0].LUTData = [0, 0, 0]
    with patch('entelaiutils.conversion.pydicom.dcmread', return_value=ds):
        with patch('entelaiutils.conversion.dcm2png_convert3') as mocked_dcm2png_convert3:
            dcm2png(filename)
            mocked_dcm2png_convert3.assert_called_once_with(filename, None)

@patch('entelaiutils.conversion.dcm2png_convert4', side_effect=ConversionError('Test'))
@patch('entelaiutils.conversion.dcm2png_convert3', side_effect=ConversionError('Test'))
def test_dcm2png_noVOILUTSequence(moked_dcm2png_convert3, mocked_dcm2png_convert4):
    filename = 'ejemplo.dcm'
    ds = Dataset()
    ds.file_meta = FileMetaDataset()
    ds. Modality = 'MG'
    with patch('entelaiutils.conversion.pydicom.dcmread', return_value=ds):
        with patch('entelaiutils.conversion.dcm2png_convert') as mocked_dcm2png_convert:
            dcm2png(filename)
            mocked_dcm2png_convert.assert_called_once_with(filename, None)

@patch('entelaiutils.conversion.dcm2png_convert', side_effect=ConversionError('Test'))
@patch('entelaiutils.conversion.dcm2png_convert2', side_effect=ConversionError('Test'))
@patch('entelaiutils.conversion.dcm2png_convert3', side_effect=ConversionError('Test'))
def test_dcm2png_noMG(moked_dcm2png_convert3, mocked_dcm2png_convert2, mocked_dcm2png_convert):
    filename = 'ejemplo.dcm'
    ds = Dataset()
    ds.file_meta = FileMetaDataset()
    ds. Modality = 'MG'
    with patch('entelaiutils.conversion.pydicom.dcmread', return_value=ds):
        with patch('entelaiutils.conversion.dcm2png_convert4') as mocked_dcm2png_convert4:
            dcm2png(filename)
            mocked_dcm2png_convert4.assert_called_once_with(filename, None)