import re
import logging
import os

from .constants import MAMMO_IMAGE_TYPES, THORAX_IMAGE_TYPES, THORAX_MODALITIES, MAMMO_MODALITIES

logger = logging.getLogger(__name__)


def has_modality(search_modality, serie):
    modality = serie.modality
    return search_modality in modality.upper()


def apply_nested_tag_filter(tag, regex, instance_t):
    """
    Search for regex match in nested dicom tags. This kind of tags are in the form:
    tag_name: [
                {tag_name2a: [
                             {tag_name_3: 'value3'
                             },
                             {tag_name_4: 'value4'
                             }]
                }]
    This function recursively walks trough the tags according to the tag passed, finally the regex matching is done
    iteratively on the last list of dictionaries

    Parameters:
         tag: string, path-like tag ('tag_name/tag_name2a/tag_name_3')
         regex: regex to match
         instance_t: instance tags in simplified version
    Return:
        result: bool, True if regex matched or tag not present in instance tags
    """
    result = True
    nested_tags = tag.split('/')
    depth = len(nested_tags)
    instance_t_local = instance_t
    for i in range(depth-1):
        if nested_tags[i] in instance_t_local.keys():
            if i != depth-2:
                instance_t_local = instance_t_local[nested_tags[i]][0]
            else:
                instance_t_local = instance_t_local[nested_tags[i]]
        else:
            return result
    found = True
    for tag_dict in instance_t_local:
        found = found and (re.search(regex, tag_dict[nested_tags[-1]], re.IGNORECASE) is not None)
    result = result and found
    return result


def apply_filter_list(filter_list, instance_t):
    result = True
    for fl in filter_list:
        tag = fl[0]
        regex = fl[1]
        if '/' in tag:
            result = result and apply_nested_tag_filter(tag, regex, instance_t)
        else:
            if tag in instance_t:
                result = result and (re.search(regex, instance_t[tag], re.IGNORECASE) is not None)
            else:
                if len(fl) > 2 and fl[2]:
                    result = False
    return result


def apply_regex_list(regex_list, instance_t):
    result = False
    for [tag, regex] in regex_list:
        if tag in instance_t:
            found = (re.search(regex, instance_t[tag], re.IGNORECASE) is not None)
            if found:
                logger.debug(f'{regex} -> {instance_t[tag]}')
            result = result or found
    return result


def get_image_type_from_instance(instance_tags, config, image_types):
    for image_type in image_types:
        for regex_dict in config[image_type]:
            if apply_filter_list(regex_dict['filter'], instance_tags) and \
                    apply_regex_list(regex_dict['regex'], instance_tags):
                return image_type
    return None


def get_mammo_image_type_from_instance(instance_tags, mg_config):    
    return get_image_type_from_instance(instance_tags, mg_config, MAMMO_IMAGE_TYPES)


def get_thorax_image_type_from_instance(instance_tags, rx_config):    
    return get_image_type_from_instance(instance_tags, rx_config, THORAX_IMAGE_TYPES)


def get_series_image_types(series, series_modality, config, dicom_server, image_types, compare_instance_number=False):
    """
    Check if the series are from the desired modality, get the image type for all instances in the series, filter
     unwanted instances using the config regex and for multiple instances of the same type, keep the newest one
     (if compare_instance_number == True, keep the instance with greater instance number instead of newest one).

    Args:
        series: pipeline Series dataclass object
        series_modality: list of allowed series modalities
        config: config dictionary included in the sqs message (contains the regex used in filtering)
        dicom_server: orthanc server to get the instances tags from
        image_types: allowed intances types
        compare_instance_number: compare two instances in the study that has the same type by instance number instead
            of AqcuisitionTime

    Returns:

    """
    series_types = {}
    for serie in series:
        if any([has_modality(mod, serie) for mod in series_modality]):
            series_types[serie.id] = {}
            logger.debug(f'Series instances: {serie.instances}')
            for instance in serie.instances:
                series_types[serie.id][instance] = None
                instance_tags = dicom_server.get_instance_tags_in_simplified_version(instance)
                instance_image_type = get_image_type_from_instance(instance_tags, config, image_types)
                if instance_image_type is not None:
                    for ser in series_types.keys():
                        for ins in series_types[ser].keys():
                            if series_types[ser][ins] is not None and \
                                    series_types[ser][ins] == instance_image_type:
                                if (compare_instance_number and
                                    dicom_server.is_instance_number_greater_than_instance_number(instance, ins)) or \
                                        dicom_server.is_instance_newer_than_instance(instance, ins):
                                    logger.info(f'This instance ({instance}) is newer than the others...')
                                    series_types[ser][ins] = None
                                else:
                                    logger.info(f'Found newer instance than this ({instance}).')
                                    instance_image_type = None

                    series_types[serie.id][instance] = instance_image_type
                else:
                    logger.info(f'All Regex failed matching instance tags. Skipping instance.')
                    logger.debug(f'{instance_tags}')
    return series_types


def _get_mammo_series_image_types(series, mg_config, dicom_server, compare_instance_number=False):
    return get_series_image_types(series, MAMMO_MODALITIES, mg_config, dicom_server, MAMMO_IMAGE_TYPES,
                                  compare_instance_number)


def _get_thorax_series_image_types(series, mg_config, dicom_server):
    return get_series_image_types(series, THORAX_MODALITIES, mg_config, dicom_server, THORAX_IMAGE_TYPES)


def _get_dicoms(series, series_modality, config, output_dir, dicom_server, series_types):

    res_dir = {}
    # se podrian recorrer las series que tengan tipo, segun _get_mammo_series_image_types
    # y bajar las instancias de las mismas
    for serie in series:
        if any([has_modality(mod, serie) for mod in series_modality]):
            logger.debug(f'Series instances: {serie.instances}')
            for instance in serie.instances:
                instance_image_type = series_types[serie.id][instance]
                if instance_image_type is not None:
                    logger.info(f'Regex matched. Downloading instance.')
                    download_dir = os.path.join(output_dir, instance_image_type)
                    os.makedirs(download_dir, exist_ok=True)
                    dicom_server.download_instance(instance, download_dir, f'{instance_image_type}.dcm')
                    res_dir[instance_image_type] = download_dir
    return res_dir


def _mammo_get_dicoms(series, mg_config, output_dir, dicom_server, series_types):
    return _get_dicoms(series, MAMMO_MODALITIES, mg_config, output_dir, dicom_server,series_types)


def _thorax_get_dicoms(series, rx_config, output_dir, dicom_server, series_types):
    return _get_dicoms(series, THORAX_MODALITIES, rx_config, output_dir, dicom_server,series_types)
