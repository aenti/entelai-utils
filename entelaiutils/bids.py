import hashlib
import os


def encrypt_string(hash_string):
    sha_signature = \
        hashlib.sha256(hash_string.encode()).hexdigest()
    return sha_signature


class BIDSFileNameResolver(object):
    """Resolves Bids Filenames
    Sigleton class to resolve filenames usign bids format rules
    """
    __instance = None

    def __str__(self):
        return self.center_name if self.center_name else ''

    def __new__(cls):
        if BIDSFileNameResolver.__instance is None:
            BIDSFileNameResolver.__instance = object.__new__(cls)
            # initial value to compatibility
            BIDSFileNameResolver.__instance.base_folder = '/entelai-temp'
        return BIDSFileNameResolver.__instance

    def set_patient_id(self, patient_id):
        self.patient_id = patient_id

    def set_base_folder(self, base_folder):
        self.base_folder = base_folder

    def set_center_name(self, center_name):
        # TODO: remove _ and - from the center_name
        self.center_name = center_name

    def set_study_id(self, study_id):
        self.study_id = study_id

    def get_base_dir(self):
        return os.path.join(self.base_folder, self.center_name, self.study_id)

    def get_sub_dir(self):
        identifier = self._get_identifier()
        sub_path = f'{identifier}/'
        base_path = os.path.join(self.get_base_dir(), sub_path)
        return base_path

    def get_derivatives_dir(self):
        identifier = self._get_identifier()
        base_path = os.path.join(self.get_base_dir(), f'derivatives/ENTELAI/{identifier}/')
        return base_path

    def make_dirs(self):
        os.makedirs(self.get_base_dir(), exist_ok=True)
        os.makedirs(self.get_sub_dir(), exist_ok=True)
        os.makedirs(self.get_derivatives_dir(), exist_ok=True)

    def _get_identifier(self):
        patient_hash = encrypt_string(self.patient_id)[0:6]
        return f'sub-{self.center_name}-{patient_hash}'

    def get_type_fname_unformatted(self, image_type, modifiers=None):
        identifier = self._get_identifier()
        if modifiers is not None:
            file_name = f'{identifier}_{image_type}_{modifiers}'
        else:
            file_name = f'{identifier}_{image_type}'

        return file_name

    def get_type_fname(self, image_type, modifiers=None):
        fname = self.get_type_fname_unformatted(image_type, modifiers)
        file_name = f'{fname}.nii.gz'
        return file_name

    def get_type_path(self, image_type, modifiers=None):
        identifier = self._get_identifier()
        sub_path = f'{identifier}/'
        base_path = os.path.join(self.get_base_dir(), sub_path)
        file_name = self.get_type_fname(image_type, modifiers)
        return os.path.join(base_path, file_name)

    def get_derivative_fname(self, image_type, modifiers, process):
        identifier = self._get_identifier()
        file_name = f'{identifier}_{image_type}_{modifiers}_{process}.nii.gz'
        return file_name

    def get_derivative_path(self, image_type, modifiers, process):
        identifier = self._get_identifier()
        sub_path = f'derivatives/ENTELAI/{identifier}/'
        base_path = os.path.join(self.get_base_dir(), sub_path)
        file_name = self.get_derivative_fname(image_type, modifiers, process)
        return os.path.join(base_path, file_name)


bids_file_resolver = BIDSFileNameResolver()
