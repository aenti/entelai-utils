import os
import glob
import logging
import subprocess
import tempfile
import pydicom
import SimpleITK as sitk
from subprocess import PIPE

import dicom2nifti

logging.basicConfig(format="%(asctime)s - %(message)s", level=logging.INFO)

logger = logging.getLogger("__name__")


file_path = os.path.realpath(__file__)
bin_path = os.path.join(os.path.dirname(file_path), "external")
os.environ["FREESURFER_HOME"] = os.environ.get("FREESURFER_HOME", bin_path)
os.environ["SUBJECTS_DIR"] = os.environ.get("SUBJECTS_DIR", file_path)


class ConversionError(Exception):
    """
    Custom error type to distinguish between know validations and script errors
    """

    def __init__(self, message):
        # Call the base class constructor with the parameters it needs
        super(ConversionError, self).__init__(message)

def nifti2dicom_explicit_wrapper(nifti_file: str, output_dicom_dir: str, dicomheaderfile: str,**kwargs):
    header = pydicom.dcmread(dicomheaderfile)
    patient_id = header['PatientID'].value if 'PatientID' in header else ''
    patient_name = header['PatientName'].value if 'PatientName' in header else ''
    patient_sex = header['PatientSex'].value if 'PatientSex' in header else ''
    study_id = header['StudyID'].value if 'StudyID' in header else ''
    studyinstanceuid = header['StudyInstanceUID'].value if 'StudyInstanceUID' in header else ''
    institution_name = header['InstitutionName'].value if 'InstitutionName' in header else ''

    keys = {'studyid': study_id,
            'studyinstanceuid': studyinstanceuid,
            'patientid': patient_id,
            'patientsex': patient_sex,
            'patientname': patient_name, 
            'institutionname': institution_name}
    nifti2dicom_wrapper(nifti_file=nifti_file, output_dicom_dir=output_dicom_dir, **keys, **kwargs)


def nifti2dicom_wrapper(nifti_file: str, output_dicom_dir: str, **kwargs):
    """Converts nifti file to dicom using nifti2dicom

    Parameters
    ----------
    nifti_file
        nifti file to be converted.

    output_dicom_dir
        output dir where dicoms instances will be stored.

    kwargs
        extra arguments for nifti2dicom:
        -# <int>,  --digits <int>
            Number of digits in dicom file names

        -s <string>,  --suffix <string>
            Suffix of dicom file names

        -p <string>,  --prefix <string>
            Prefix of dicom file names

        -o <string>,  --outputdirectory <string>
            (required)  Output dicom directory

        -r,  --rescale
            Rescale image before exporting

        -i <string>,  --inputfile <string>
            (required)  Input NIFTI 1 file

        --acquisitiontime <AAAAMMGG>
            (0008,0032) Acquisition Time

        --acquisitiondate <AAAAMMGG>
            (0008,0022) Acquisition Date

        --acquisitionnumber <string>
            (0020,0012) Acquisition Number

        --useoriginalseries
            Use original series

        --seriestime <AAAAMMGG>
            (0008,0031) Series Time

        --seriesdate <AAAAMMGG>
            (0008,0021) Series Date

        --seriesdescription <string>
            (0008,103e) Series Description

        --seriesnumber <string>
            (0020,0011) Series Number

        --seriesinstanceuid <string>
            (0020,000e) Series Instance UID

        --donotuseoriginalstudy
            Do not use original study

        --studytime <hhmmss>
            (0008,0030) Study Time

        --studydate <YYYYMMDD>
            (0008,0020) Study Date

        --studydescription <string>
            (0008,1030) Study Description

        --studyid <string>
            (0020,0010) Study ID

        --studyinstanceuid <string>
            (0020,000d) Study Instance UID

        --patientweight <WW>
            (0010,1030) Patient Weight

        --patientage <###Y/M>
            (0010,1010) Patient Age

        --patientsex <M|F|O>
            (0010,0040) Patient Sex

        --patientdob <AAAAMMGG>
            (0010,0030) Patient Date of Birth

        --patientid <string>
            (0010,0020) Patient ID

        --patientname <COGNOME NOME>
            (0010,0010) Patient Name

        --referringphysiciansname <string>
            (0008,0090) Referring Physician's Name

        --institutionname <string>
            (0008,0080) Institution Name

        --manufacturersmodelname <string>
            (0008,1090) Manufacturer's Model Name

        --manufacturer <string>
            (0008,0070) Manufacturer

        --protocolname <string>
            (0018,1030) Protocol Name

        --softwareversion <string>
            (0018,1020) Software Version(s)

        --imagetype <string>
            (0008,0008) Image Type

        --modality <string>
            (0008,0060) Modality

        --sopclassuid <string>
            (0008,0016) SOP Class UID

        -d <string>,  --dicomheaderfile <string>
            File containing DICOM header to import

        -y,  --yes
            Do not prompt for Accession Number Warning

        -a <string>,  --accessionnumber <string>
            (0008,0050) Accession Number

        --,  --ignore_rest
            Ignores the rest of the labeled arguments following this flag.

        --version
            Displays version information and exits.

        -h,  --help
            Displays usage information and exits.
    Example
    --------
    >>> from entelaiutils import conversion
    >>> nifti_file = '/home/user/3DT1_aparc+aseg.nii.gz'
    >>> output_dicom_dir = '/home/user/3DT1/fs_dicom_dir'
    >>> dicom_header_file = '/home/user/3DT1/1.dcm'
    >>> conversion.nifti2dicom_wrapper(dicom_dir, output_file, dicomheaderfile=dicom_header_file)
    """
    logger.info(f"Running nifti2dicom with args {kwargs}...")
    command = [
        f"nifti2dicom",
        "--inputfile",
        nifti_file,
        "--outputdirectory",
        output_dicom_dir,
        "--yes",
    ]
    for k, v in kwargs.items():
        command.append(f"--{k}")
        command.append(f"{v}")

    try:
        output = subprocess.run(command, stdout=PIPE, stderr=PIPE)
        output.check_returncode()
        return output.stdout.decode("utf-8")
    except Exception as e:
        if output:
            raise ConversionError(
                f"nifti2dicom failed: {e}, \n output: {output.stdout.decode('utf-8')} {output.stderr.decode('utf-8')}."
            )
        else:
            raise ConversionError(f"nifti2dicom failed failed: {e}")

def simpleitk_wrapper(dicom_dir: str, output_file: str, **kwargs):
    logger.info("Running dicom2nifti...")
    output = None
    try:
        data_directory = dicom_dir
        series_IDs = sitk.ImageSeriesReader.GetGDCMSeriesIDs(data_directory)
        if not series_IDs:
            print("ERROR: given directory \""+data_directory+"\" does not contain a DICOM series.")
            raise
        series_file_names = sitk.ImageSeriesReader.GetGDCMSeriesFileNames(data_directory, series_IDs[0])

        series_reader = sitk.ImageSeriesReader()
        series_reader.SetFileNames(series_file_names)
        series_reader.MetaDataDictionaryArrayUpdateOn()
        series_reader.LoadPrivateTagsOn()
        image3D = series_reader.Execute()

        sitk.WriteImage(image3D, output_file)
        
    except Exception as e:
        if output:
            raise ConversionError(f"simpleitk failed: {e}, \n output: {output}.")
        else:
            raise ConversionError(f"simpleitk failed: {e}")


def dicom2nifti_wrapper(dicom_dir: str, output_file: str, **kwargs):
    logger.info("Running dicom2nifti...")
    output = None
    try:
        output = dicom2nifti.dicom_series_to_nifti(
            dicom_dir, output_file, reorient_nifti=True, **kwargs
        )
    except Exception as e:
        if output:
            raise ConversionError(f"dicom2nifti failed: {e}, \n output: {output}.")
        else:
            raise ConversionError(f"dicom2nifti failed: {e}")


def mri_convert_to_1mm(seg_nifti_file: str, seg: bool, output_file: str):
    if seg:
        command = [
            f"{bin_path}/mri_convert.bin",
            seg_nifti_file,
            output_file,
            "-c",
            "-rt",
            "nearest",
            "-ns",
            "1",
            "-nc",
        ]
    else:
        command = [
            f"{bin_path}/mri_convert.bin",
            seg_nifti_file,
            output_file,
            "-c",
        ]

    try:
        output = subprocess.run(command, stdout=PIPE, stderr=PIPE)
        output.check_returncode()
        return output.stdout.decode("utf-8")
    except Exception as e:
        if output:
            raise ConversionError(
                f"mri_convert seg to 1mm failed: {e}, \n output: {output.stdout.decode('utf-8')} {output.stderr.decode('utf-8')}."
            )
        else:
            raise ConversionError(f"mri_convert seg to 1mm failed: {e}")


def mri_convert_wrapper(dicom_dir: str, output_file: str, **kwargs):
    """Converts dicoms to nifti file using FS mri_convert

    Parameters
    ----------
    dicom_dir
        dicom dir where dicoms files are stored.

    output_file
        output nifti file.

    kwargs
        extra arguments for mri_convert. Requires short version:
        for example: -c instead of --conform

    Example
    --------
    >>> from entelaiutils import conversion
    >>> dicom_dir = '/home/user/3DT1/dicom_dir'
    >>> output_file = '/home/user/3DT1.nii.gz'
    >>> conversion.mri_convert_wrapper(dicom_dir, output_file)
    """
    logger.info("Running mri_convert...")
    output = None
    first_dicom = sorted(glob.glob(f"{dicom_dir}/*"))[0]
    command = [f"{bin_path}/mri_convert.bin", first_dicom, output_file]

    for k, v in kwargs.items():
        command.append(f"-{k}")
        command.append(f"{v}")

    try:
        output = subprocess.run(command, stdout=PIPE, stderr=PIPE)
        output.check_returncode()
        return output.stdout.decode("utf-8")
    except Exception as e:
        if output:
            raise ConversionError(
                f"mri_convert failed: {e}, \n output: {output.stdout.decode('utf-8')} {output.stderr.decode('utf-8')}."
            )
        else:
            raise ConversionError(f"mri_convert failed: {e}")


def dcm2niix_wrapper(dicom_dir: str, output_file: str, **kwargs):
    """Converts dicoms to nifti file using dcm2niix

    Parameters
    ----------
    dicom_dir
        dicom dir where dicoms files are stored.

    output_file
        output nifti file.

    extra_params
        extra arguments for mri_convert.

    Example
    --------
    >>> from entelaiutils import conversion
    >>> dicom_dir = '/home/user/3DT1/dicom_dir'
    >>> output_file = '/home/user/3DT1.nii.gz'
    >>> conversion.dcm2niix(dicom_dir, output_file,'')
    """
    logger.info("Running dcm2niix...")
    output = None
    command = [
        f"{bin_path}/dcm2niix",
        "-o",
        os.path.dirname(output_file),
        "-f",
        os.path.basename(output_file).split(".")[0],
        "-z",
        "y",
        dicom_dir,
    ]

    for k, v in kwargs.items():
        command.append(f"-{k}")
        command.append(f"{v}")

    try:
        output = subprocess.run(command, stdout=PIPE, stderr=PIPE)
        output.check_returncode()
        return output.stderr.decode("utf-8")
    except Exception as e:
        if output:
            raise ConversionError(
                f"dcm2niix failed: {e}, \n output: {output.stderr.decode('utf-8')}."
            )
        else:
            raise ConversionError(f"dcm2niix failed: {e}")


def gdcm_preprocess(input_dicom_dir: str, output_dicom_dir: str):
    """Preprocess files to RAW dicom syntax format

    Parameters
    ----------
    inptu_dicom_dir
        dicom dir where dicoms files are stored.
    output_dicom_dir
        dir where newly generated files are writen to.


    Example
    --------
    >>> from entelaiutils import conversion
    >>> dicom_dir = '/home/user/3DT1/dicom_dir'
    >>> conversion.gdcm_preprocess(dicom_dir)
    """
    output = None
    logger.info("Running gdcm_preprocess...")
    input_files = glob.glob(os.path.join(input_dicom_dir, "*.dcm"))
    for file in input_files:
        output_file = os.path.join(output_dicom_dir, os.path.basename(file))
        command = ["gdcmconv", "-V", "-i", file, "-o", output_file, "-w"]
        try:
            output = subprocess.run(command, stdout=PIPE, stderr=PIPE)
            output.check_returncode()
        except Exception as e:
            if output:
                raise ConversionError(
                    f"gdcm preprocess failed: {e}, \n output: {output.stderr.decode('utf-8')}."
                )
            else:
                raise ConversionError(f"gdcm preprocess failed: {e}")


def gdcm_before_dcm2niix_wrapper(dicom_dir: str, output_file: str):
    """Preprocess dicom files with gdcm before converting them to nifti using dcm2niix
    This is useful when the image information is encapsulated using JPG-lossless

    WARNING: This function requires GDCM installed to work properly (at the moment there is no easy way to include
    it as a requirement).

    Parameters
    ----------
    dicom_dir
        dicom dir where dicoms files are stored.

    output_file
        output nifti file.

    Example
    --------
    >>> from entelaiutils import conversion
    >>> dicom_dir = '/home/user/3DT1/dicom_dir'
    >>> output_file = '/home/user/3DT1.nii.gz'
    >>> conversion.gdcm_before_dcm2niix_wrapper(dicom_dir, output_file)
    """
    logger.info("Running gdcm_before_dcm2niix conversion...")
    # with tempfile.TemporaryDirectory() as temp_dir:
    #    gdcm_preprocess(dicom_dir, temp_dir)
    #    output = dcm2niix_wrapper(temp_dir, output_file)
    conv_dir = dicom_dir.rstrip("/") + "_raw"
    os.mkdir(conv_dir)
    gdcm_preprocess(dicom_dir, conv_dir)
    output = dcm2niix_wrapper(conv_dir, output_file)
    return output


def dicom_to_nifti(dicom_dir: str, output_file: str, **kwargs):
    """Converts dicoms to nifti file.
    It trys different approaches in following order mri_convert -> dcm2niix -> dicom2nifti

    Parameters
    ----------
    dicom_dir
        dicom dir where dicoms files are stored.

    output_file
        output nifti file.

    Example
    --------
    >>> from entelaiutils import conversion
    >>> dicom_dir = '/home/user/3DT1/dicom_dir'
    >>> output_file = '/home/user/3DT1.nii.gz'
    >>> conversion.dicom_to_nifti(dicom_dir, output_file)
    """
    methods = [
        simpleitk_wrapper,
        dicom2nifti_wrapper,
        dcm2niix_wrapper,
        mri_convert_wrapper,
        gdcm_before_dcm2niix_wrapper,
    ]
    for method in methods:
        try:
            method(dicom_dir, output_file, **kwargs)
            logger.info(f"Conversion done with {method.__name__}")
            break

        except ConversionError as e:
            logger.error(e)
            continue


def nifti_to_dicom(nifti_file: str, output_dicom_dir: str, **kwargs):
    """Converts nifti file to dicom

    Parameters
    ----------
    nifti_file
        nifti file to be converted.

    output_dicom_dir
        output dir where dicoms instances will be stored.

    kwargs
        extra arguments

    Example
    --------
    >>> from entelaiutils import conversion
    >>> nifti_file = '/home/user/3DT1_aparc+aseg.nii.gz'
    >>> output_dicom_dir = '/home/user/3DT1/fs_dicom_dir'
    >>> dicom_header_file = '/home/user/3DT1/1.dcm'
    >>> conversion.nifti_to_dicom(dicom_dir, output_file, dicomheaderfile=dicom_header_file)
    """
    methods = [nifti2dicom_wrapper, nifti2dicom_explicit_wrapper]
    for method in methods:
        try:
            output = method(nifti_file, output_dicom_dir, **kwargs)
            logger.info(f"Conversion done with output {output}")
            break

        except ConversionError as e:
            logger.error(e)
            continue



def dcm2png_preprocess_gdcm_raw(filename):
    try:
        filename_raw = filename[:-4] + "_raw.dcm"
        command = ["gdcmconv", "--raw", filename, filename_raw ]
        output = subprocess.run(command, stdout=PIPE, stderr=PIPE)
        output.check_returncode()
        return_value = filename_raw
    except Exception as e:
        if output:
            logger.info(f"gdcm preprocess failed: {e}, \n output: {output.stderr.decode('utf-8')}.")
            
        else:
            logger.info(f"gdcm preprocess failed: {e}")
        return_value = filename
    return return_value

def dcm2png_convert(filename, output_fn=None):
    if output_fn is None:
        output_fn = filename[:-4] + ".png"

    try:
        raw_file = dcm2png_preprocess_gdcm_raw(filename)
        command = ["convert", raw_file, output_fn]
        output = subprocess.run(command, stdout=PIPE, stderr=PIPE)
        output.check_returncode()
    except Exception as e:
        if output:
            raise ConversionError(
                f"dcm2png_convert failed: {e}, \n output: {output.stderr.decode('utf-8')}."
            )
        else:
            raise ConversionError(f"dcm2png_convert preprocess failed: {e}")

    return output_fn

def dcm2png_convert_pgm(filename, output_fn=None):
    if output_fn is None:
        output_fn = filename[:-4] + ".png"

    try:
        raw_file = dcm2png_preprocess_gdcm_raw(filename)
        command = [
            "gdcmimg",
            raw_file,
            filename[:-4] + "_raw.dcm.pgm",
        ]
        output = subprocess.run(command, stdout=PIPE, stderr=PIPE)
        output.check_returncode()
        command = ["convert", "-auto-level", filename[:-4] + "_raw.dcm.pgm", output_fn]
        output = subprocess.run(command, stdout=PIPE, stderr=PIPE)
        output.check_returncode()
    except Exception as e:
        if output:
            raise ConversionError(
                f"dcm2png_convert_pgm preprocess failed: {e}, \n output: {output.stderr.decode('utf-8')}."
            )
        else:
            raise ConversionError(f"dcm2png_convert_pgm preprocess failed: {e}")

    return output_fn


def dcm2png_dcm2pnm(filename, output_fn=None, voi_lut_seq_or_window_number=1):
    """
    Use dcmtk as conversion function, which allows use of VOI LUT intensity correction. Check if VOILUTSequence tag is
    present in the dicom header; if true, use the passed number of VOI LUT sequence or the first one as default in the
    conversion.

    Args:
        filename: filepath to the dicom image
        output_fn: filepath to the png image
        voi_lut_seq_or_window_number: VOI LUT sequence number to use
    Returns:
        output_fn

    """
    try:
        assert voi_lut_seq_or_window_number != 0
    except AssertionError:
        logger.exception('Voi lut or window number must be different to 0')
        raise
    
    if output_fn is None:
        output_fn = filename[:-4] + '.png'

    filename_raw = dcm2png_preprocess_gdcm_raw(filename)

    dicom_header = pydicom.dcmread(
        filename_raw, specific_tags=['VOILUTSequence', 'WindowCenterWidthExplanation', 'WindowWidth', 'WindowCenter']
    )
    if hasattr(dicom_header, 'VOILUTSequence'):
        command = [
            'dcm2pnm', '+on2', '+Wl', str(voi_lut_seq_or_window_number), filename_raw, output_fn
        ]
    elif hasattr(dicom_header, 'WindowCenterWidthExplanation'):
        command = [
            'dcm2pnm', '+on2', '+Wi', str(voi_lut_seq_or_window_number), filename_raw, output_fn
        ]
    elif hasattr(dicom_header, 'WindowCenter') and hasattr(dicom_header, 'WindowWidth'):
        command = [
            'dcm2pnm', '+on2', '+Ww', str(dicom_header.WindowCenter), str(dicom_header.WindowWidth), filename_raw, output_fn
        ]
    else:
        command = [
            'dcm2pnm', '+on2', filename_raw, output_fn
        ]
    output = None
    try:
        output = subprocess.run(command, stdout=PIPE, stderr=PIPE)
        output.check_returncode()
    except Exception as e:
        if output:
            raise ConversionError(
                f"dcm2pnm preprocess failed: {e}, \n output: {output.stderr.decode('utf-8')}.")
        else:
            raise ConversionError(f'dcm2pnm preprocess failed: {e}')
    return output_fn


def dcm2png_dcmj2pnm(filename, output_fn=None, voi_lut_seq_or_window_number=1):
    """
    Use dcmtk as conversion function, which allows use of VOI LUT intensity correction. Check if VOILUTSequence tag is
    present in the dicom header; if true, use the passed number of VOI LUT sequence or the first one as default in the
    conversion.

    Args:
        filename: filepath to the dicom image
        output_fn: filepath to the png image
        voi_lut_seq_or_window_number: VOI LUT sequence number to use or number of window to use
    Returns:
        output_fn

    """
    try:
        assert voi_lut_seq_or_window_number != 0
    except AssertionError:
        logger.exception('Voi lut or window number must be different to 0')
        raise

    if output_fn is None:
        output_fn = filename[:-4] + '.png'

    filename_raw = dcm2png_preprocess_gdcm_raw(filename)

    dicom_header = pydicom.dcmread(
        filename_raw, specific_tags=['VOILUTSequence', 'WindowCenterWidthExplanation', 'WindowWidth', 'WindowCenter']
    )
    if hasattr(dicom_header, 'VOILUTSequence'):
        command = [
            'dcmj2pnm', '+on2', '+Wl', str(voi_lut_seq_or_window_number), filename_raw, output_fn
        ]
    elif hasattr(dicom_header, 'WindowCenterWidthExplanation'):
        command = [
            'dcmj2pnm', '+on2', '+Wi', str(voi_lut_seq_or_window_number), filename_raw, output_fn
        ]
    elif hasattr(dicom_header, 'WindowCenter') and hasattr(dicom_header, 'WindowWidth'):
        command = [
            'dcmj2pnm', '+on2', '+Ww', str(dicom_header.WindowCenter), str(dicom_header.WindowWidth), filename_raw, output_fn
        ]
    else:
        command = ['dcmj2pnm', '+on2', filename_raw, output_fn]

    output = None
    try:
        output = subprocess.run(command, stdout=PIPE, stderr=PIPE)
        output.check_returncode()
    except Exception as e:
        if output:
            raise ConversionError(
                f"dcmj2pnm preprocess failed: {e}, \n output: {output.stderr.decode('utf-8')}.")
        else:
            raise ConversionError(f'dcmj2pnm preprocess failed: {e}')
    return output_fn


def dcm2png(filename, output_fn=None):
    """
    Convert a .dcm 2D image into a .png one. If the dicom image has the VOILUTSequence tag use specifically
    dcm2png_convert3 converter, if not try all the ones available until one works.

    Args:
        filename: filepath to the dicom image
        output_fn: filepath to the png image

    Returns:

    """

    dicom_header = pydicom.dcmread(
        filename, specific_tags=['VOILUTSequence', 'WindowCenterWidthExplanation', 'Modality']
    )
    if (hasattr(dicom_header, 'Modality') and (dicom_header.Modality=='MG')):
        if (hasattr(dicom_header, 'VOILUTSequence') or 
            hasattr(dicom_header, 'WindowCenterWidthExplanation')):
            methods = [dcm2png_dcmj2pnm, dcm2png_dcm2pnm]
        else:
            methods = [dcm2png_dcmj2pnm, dcm2png_dcm2pnm, dcm2png_convert, dcm2png_convert_pgm]
    else:
        methods = [dcm2png_dcmj2pnm, dcm2png_dcm2pnm, dcm2png_convert, dcm2png_convert_pgm]

    for method in methods:
        try:
            output = method(filename, output_fn)
            logger.info(f"Conversion done with output {output}")
            break

        except ConversionError as e:
            logger.exception(e)
            continue

    return output
